// перше завдання
function getNumber(promptText) {
  let number;
  do {
    number = prompt(promptText);
  } while (isNaN(number));
  return Number(number);
}

let number1 = getNumber("Введіть перше число: ");
let number2 = getNumber("Введіть друге число: ");

if (number1 > number2) {
  [number1, number2] = [number2, number1];
}

for (let i = Math.floor(number1); i <= Math.floor(number2); i++) {
  console.log(i);
}

// друге завдання
function getEvenNumber(promptText) {
  let number;
  do {
    number = prompt(promptText);
  } while (isNaN(number) || number % 2 !== 0);
  return Number(number);
}

let evenNumber = getEvenNumber("Введіть парне число: ");
console.log(`Ви ввели парне число: ${evenNumber}`);
